﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SocketIO;
using System;

namespace Project.Networking
{
    public class NetworkClient : SocketIO.SocketIOComponent
    {
        [Header("Network Client")]
        [SerializeField]
        private Transform networkContainer;

        private Dictionary<string, NetworkIdentity> serverObjects;
        private Dictionary<string, int> serverObjectsSteps;


        public Transform playerSpawnPoint0;
        public Transform playerSpawnPoint1;
        public Transform ballSpawnPoint;
        public GameObject playerPrefab;
        public GameObject otherPlayerPrefab;
        public GameObject ballPrefab;

        public static string ClientID { get; private set; }


        // Start is called before the first frame update
        public void Start()
        {
            base.Start();
            initialise();
            SetupEvents();
        }

        private void initialise()
        {
            serverObjects = new Dictionary<string, NetworkIdentity>();
            serverObjectsSteps = new Dictionary<string, int>();
        }

        private void SetupEvents()
        {
            On("open", (E) => 
            {
                Debug.Log("Connection made to the server.");
            });

            On("register", (E) => 
            {
                ClientID = E.data["id"].ToString().Replace("'", "");
                Debug.LogFormat("Our Client's ID is ({0})", ClientID);
            });

            On("spawnPlayer", (E) =>
            {
                string id = E.data["id"].ToString().Replace("'", "");
                float playerIndex = E.data["playerIndex"].f;

                GameObject go = Instantiate(playerPrefab);
                go.name = string.Format("Player ({0})", id);

                if (playerIndex == 0)
                    go.transform.position = playerSpawnPoint0.position;
                else
                    go.transform.position = playerSpawnPoint1.position;

                NetworkIdentity ni = go.GetComponent<NetworkIdentity>();
                ni.SetControllerID(id);
                ni.SetSocketReference(this);

                serverObjects.Add(id, ni);
            });

            On("spawn", (E) =>
            {
                string id = E.data["id"].ToString().Replace("'", "");
                float pos_x = E.data["position"]["x"].f;
                float pos_y = E.data["position"]["y"].f;

                GameObject go = Instantiate(otherPlayerPrefab);
                go.name = string.Format("Player ({0})", id);
                go.transform.position = new Vector3(pos_x, pos_y, 0);
                go.SetActive(false);

                NetworkIdentity ni = go.GetComponent<NetworkIdentity>();
                ni.SetControllerID(id);
                ni.SetSocketReference(this);

                serverObjects.Add(id, ni);
            });

            On("spawnBall", (E) =>
            {
                string id = E.data["id"].ToString().Replace("'", "");
                float pos_x = E.data["position"]["x"].f;
                float pos_y = E.data["position"]["y"].f;
/*
                Debug.Log("Spawning Ball");*/

                if (!GameObject.FindGameObjectWithTag("Ball"))
                {

                    GameObject go = Instantiate(ballPrefab);
                    go.name = string.Format("Ball ({0})", id);
                    go.transform.position = new Vector3(pos_x, pos_y, 0);


                    NetworkIdentity ni = go.GetComponent<NetworkIdentity>();
                    ni.SetControllerID(id);
                    ni.SetSocketReference(this);

                    serverObjects.Add(id, ni);
                }
                else
                {
                    BallController.instance.gameObject.SetActive(true);
                }
            });

            On("updatePlayer", (E) =>
            {
                string id = E.data["id"].ToString().Replace("'", "");
                float pos_x = E.data["position"]["x"].f;
                float pos_y = E.data["position"]["y"].f;

                float vel_x = E.data["velocity"]["x"].f;
                float vel_y = E.data["velocity"]["y"].f;

                string ballControl = E.data["ballControl"].ToString().Replace("'", "");

                if (ballControl == "true" && !BallController.instance.GetComponent<NetworkIdentity>().IsControlling())
                {
                    BallController.instance.gameObject.GetComponent<NetworkIdentity>().isControlling = true;
                    BallController.instance.gameObject.GetComponent<NetworkTransform>().enabled = true;
                    BallController.instance.gameObject.GetComponent<SpriteRenderer>().color = new Color(255, 0, 0);
                    StartCoroutine(BallController.instance.ResetBall(ballSpawnPoint.position));
                }


                NetworkIdentity ni = serverObjects[id];
                ni.gameObject.SetActive(true);

                /*if (ni.gameObject.name.Contains("Ball")) {
                    Debug.Log("Updating Ball");
                    ni.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(vel_x, vel_y);

                    Vector3 newPos = new Vector3(pos_x, pos_y, 0);
                    Vector3 deltaPos = ni.gameObject.transform.position - newPos;

                    if (deltaPos.magnitude >= 0.5)
                        ni.gameObject.transform.position = newPos;
                }*/
                if (!ni.IsControlling())
                {
                    OtherPlayerController otherPlayerController = ni.gameObject.GetComponent<OtherPlayerController>();

                    Vector3 newPos = new Vector3(pos_x, pos_y, 0);
                    Vector3 deltaPos = otherPlayerController.transform.position - newPos;

                    /*if (deltaPos.magnitude >= 0.2)*/
                    ni.gameObject.transform.position = newPos;
                }
            });


            On("updateBall", (E) =>
            {
                string id = E.data["id"].ToString().Replace("'", "");
                float pos_x = E.data["position"]["x"].f;
                float pos_y = E.data["position"]["y"].f;

                float vel_x = E.data["velocity"]["x"].f;
                float vel_y = E.data["velocity"]["y"].f;

                /*Debug.Log("Updating Ball");
*/

                NetworkIdentity ni = serverObjects[id];
                ni.gameObject.SetActive(true);

                if (!ni.IsControlling())
                {
                    Debug.Log("Updating Ball");
                    Vector3 newPos = new Vector3(pos_x, pos_y, 0);
                    Vector3 deltaPos = ni.gameObject.transform.position - newPos;

                    /*if (deltaPos.magnitude >= 2.0)*/
                        ni.gameObject.transform.position = newPos;

                    Vector3 newVel = new Vector3(vel_x, vel_y, 0);
                    Vector3 deltaVel = ni.gameObject.transform.position - newVel;

                    /*if (deltaVel.magnitude >= 2.0)*/
                        BallController.instance.rb.velocity = newVel;
                }
            });


            On("hideBall", (E) =>
            {
                BallController.instance.gameObject.SetActive(false);
            });

            On("disconnected", (E) =>
            {
                string id = E.data["id"].ToString().Replace("'", "");

                NetworkIdentity ni = serverObjects[id];
                GameObject go = ni.gameObject;
                Destroy(go);
                Destroy(ni);
                serverObjects.Remove(id);
            });
        }
    }

    [Serializable]
    public class Player
    {
        public Vector2 position;
        public Vector2 velocity;
        public int playerIndex;
    }
}