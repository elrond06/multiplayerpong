﻿using System.Collections;
using Project.Networking;
using UnityEngine;

public class BallController : MonoBehaviour
{
    public static BallController instance;

    public Rigidbody2D rb;
    public Vector2 initialVelocity;
    public Vector3 initialPosition;
    public float moveSpeed;
    public float respawnTime = 0.0f;
    // Start is called before the first frame update

    public bool collided = true;

    private void Awake()
    {
        instance = this;
    }

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponent<NetworkIdentity>().IsControlling())
        {
            if ((transform.position - initialPosition).magnitude >= 50)
            {
                StartCoroutine(ResetBall(initialPosition));
            }
        }
    }

    public IEnumerator ResetBall(Vector3 initialPosition)
    {
        yield return new WaitForSeconds(respawnTime);

        if (GetComponent<NetworkIdentity>().IsControlling())
        {
            this.initialPosition = initialPosition;
            transform.position = initialPosition;
            initialVelocity = new Vector2(Random.insideUnitCircle.normalized.y * moveSpeed, (Random.Range(0, 2) * 2 - 1) * moveSpeed);
            rb.velocity = initialVelocity;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (GetComponent<NetworkIdentity>().IsControlling())
        {
            if (other.gameObject.CompareTag("Player"))
            {
                Vector2 vel;
                vel.x = rb.velocity.x;
                vel.y = (rb.velocity.y / 2) + (other.GetComponent<Rigidbody2D>().velocity.y / 3);
                rb.velocity = vel;
            }

            collided = true;
        }
    }
}
