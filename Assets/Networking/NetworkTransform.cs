﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Project.Networking
{
    [RequireComponent(typeof(NetworkIdentity))]
    public class NetworkTransform : MonoBehaviour
    {
        [SerializeField]
        private Vector3 oldPosition;

        private NetworkIdentity networkIdentity;
        private Player player;

        private float stillCounter = 0;

        public void Awake()
        {
            networkIdentity = GetComponent<NetworkIdentity>();
        }

        public void Start()
        {
            oldPosition = transform.position;
            player = new Player();
            player.velocity = new Vector2(0, 0);

            if (!networkIdentity.IsControlling())
            {
                enabled = false;
            }
        }

        public void Update()
        {
            if (networkIdentity.IsControlling())
            {
                if (oldPosition != transform.position)
                {                    
                    oldPosition = transform.position;
                    stillCounter = 0;

                    if (gameObject.CompareTag("Ball") /*&& BallController.instance.collided*/)
                        SendDataBall();
                    else
                        SendData();
                }
                else
                {
                    stillCounter += Time.deltaTime;

                    if (stillCounter >= 1)
                    {
                        stillCounter = 0;

                        if (gameObject.CompareTag("Ball") /*&& BallController.instance.collided*/)
                            SendDataBall();
                        else
                            SendData();
                    }
                }
            }
        }

        private void SendData()
        {
            if (!gameObject.name.Contains("Ball"))
            {
                PlayerController playerController = PlayerController.instance;
                player.position = playerController.gameObject.transform.position;
                player.velocity = new Vector2(0, 0);
                networkIdentity.GetSocket().Emit("updatePlayer", new JSONObject(JsonUtility.ToJson(player)));
            }
        }

        public void SendDataBall()
        {
            if (gameObject.name.Contains("Ball"))
            {
                BallController ballController = BallController.instance;
                player.position = ballController.gameObject.transform.position;
                player.velocity = ballController.rb.velocity;
                networkIdentity.GetSocket().Emit("updateBall", new JSONObject(JsonUtility.ToJson(player)));

                BallController.instance.collided = false;
            }
        }
    }
}
