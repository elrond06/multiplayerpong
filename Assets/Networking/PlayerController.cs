﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public static PlayerController instance;

    /*public Rigidbody2D theRB;*/
    public SpriteRenderer spriteRenderer;
    public float moveSpeed;
    public int playerIndex;

    private void Awake()
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        /*theRB = GetComponent<Rigidbody2D>();*/
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (GameObject.Find("Fixed Joystick"))
        {
            GameObject joystick = GameObject.Find("Fixed Joystick");
            float input = joystick.GetComponent<FixedJoystick>().Horizontal;

            
            gameObject.transform.position += new Vector3(input * 0.75f * moveSpeed, 0, 0);
        }
        else
        {
            if (Input.GetKey(KeyCode.LeftArrow))
                /*theRB.velocity = new Vector2(theRB.velocity.x, -moveSpeed);*/
                gameObject.transform.position += new Vector3(-moveSpeed, 0);
            else if (Input.GetKey(KeyCode.RightArrow))
                gameObject.transform.position += new Vector3(moveSpeed, 0);
            /*theRB.velocity = new Vector2(theRB.velocity.x, moveSpeed);*/
        }
    }
}